export type AtPayload = {
    sub : string;
    email : string;
}
export type SuccessErrorType = "success" | "error"

export type RtPayload = AtPayload & {refresh_token : string}

export type At_Rt = {
    access_token : string;
    refresh_token : string;
    status? : SuccessErrorType
}

export type LoginType = {
    access_token : string;
    refresh_token : string;
    email : string;
    firstName : string;
    lastName : string;
    status : SuccessErrorType;
    twoFactors : boolean;
}

export enum TypesType {
    fix,
    variable
}
export enum FrequenceType{
    daily,
    weekly,
    monthly,
    yearly
}

export type ResponseTypes = {
    message : string;
    status : SuccessErrorType;
    error : string | null;
}

