import { Schema } from "mongoose";
import * as bcrypt from "bcrypt";
import { CapitalizeFirstLetter, LowerCaseString } from "src/utils";

export interface User {
    _id : string;
    firstName : string;
    lastName : string;
    email : string;
    password : string;
    verified : boolean;
    token : string | null;
    refresh_token : string | null;
    temp_secret? : {
        ascii : string;
        hex : string;
        base32 : string;
        otpauth_ur : string;
    };
    data_url? : string;
    twoFactors : boolean;
}

// creating user schema
export const UserSchema = new Schema({
    firstName : {
        type : String,
        required : true
    },
    lastName : {
        type : String,
        required : true
    },
    email : {
        type : String,
        required : true
    },
    password : {
        type : String,
        required : true
    },
    verified : {
        type : Boolean,
        default : false
    },
    token : {
        type : String
    },
    refresh_token : {
        type : String
    },
    temp_secret : {
        ascii : String,
        hex : String,
        base32 : String,
        otpauth_url : String
    },
    data_url : {
        type : String
    },
    twoFactors : {
        type : Boolean,
        default : false
    }
});

// hash password before storing it to the database
UserSchema.pre<User>("save", function (next : Function){
    const user = this;
        if(user){
            // capitalize first letter
            user.firstName = CapitalizeFirstLetter(user.firstName);
            // capitalize first letter
            user.lastName = CapitalizeFirstLetter(user.lastName);
            // lowercase all caracters
            user.email = LowerCaseString(user.email);
            // generate salt
            const salt = bcrypt.genSaltSync(10);
            // generate hash
            const hash = bcrypt.hashSync(user.password, salt);
            // storing hash as password
            user.password = hash;
            next()
        }
})
