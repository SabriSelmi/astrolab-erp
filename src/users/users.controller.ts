import { Body, Controller, Post, Req, Query, Get, Delete } from '@nestjs/common';
import { Request } from 'express';
import { CurrentUser, Public } from 'src/auth/decorators';
import { ResponseTypes } from 'src/interfaces/interface';
import { UsersService } from './users.service';
import { User } from './user.model';

@Public() // Bypass the AuthGuardRoutes
@Controller('users')
export class UsersController {
    constructor(private readonly usersService : UsersService){}
    @Post() // Register a new user
    addUser(
        @Body("firstName") firstName: string,
        @Body("lastName") lastName: string,
        @Body("email") email: string,
        @Body("password") password: string,
        @Req() req : Request
    ){
        
        return this.usersService.addUser(firstName, lastName, email, password, req);
    }
    @Delete() // Delete a user
    deleteUser(@CurrentUser() user : User){        
        return this.usersService.deleteUser(user._id);
    }

    @Get("verify") // Verify the user email account
    verifyUser(
        @Query("token") token : string
    ) : Promise<ResponseTypes>{
        return this.usersService.verifyUserMail(token)
    }
}
