import { ConflictException, Injectable, InternalServerErrorException, NotFoundException, BadRequestException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import {v4 as uuid} from "uuid";
import { ResponseTypes } from 'src/interfaces/interface';
import { MailService } from 'src/mail/mail.service';
import {  successResponse } from 'src/utils';
import { User } from './user.model';
import validator from 'validator';
import { ConfigService } from '@nestjs/config';
import { Request } from 'express';


@Injectable()
export class UsersService {
    constructor(
        @InjectModel("User") private readonly userModel : Model<User>,
        private mailService : MailService,
        private configService : ConfigService
    ){}

    // Find a verified user with his email
    async findUser(email : string) : Promise<User>{
        try {
            const user = await this.userModel.findOne({$and:[{email},{verified : true}]});
            return user
        } catch (error) {
            return undefined
        }
    }

    // Find a user with his email
    async findIncludedUnverifiedUser(email : string) : Promise<User>{
        try {
            const user = await this.userModel.findOne({email});
            return user
        } catch (error) {
            return undefined
        }
    }

    // Find a user by his id
    async findUserById(id : string) : Promise<User>{
        try {
            const user = await this.userModel.findById(id);
            return user
        } catch (error) {
            return null
        }
    }

    // Register a new User
    async addUser(
        firstName : string,
        lastName : string,
        email : string,
        password : string,
        req : Request
    ) : Promise<ResponseTypes>{
        try {
            const host = this.configService.get("ENVIRONMENT") === "dev" ? "http://localhost:3000" : `${req.protocol}://${req.get('Host')}`; // For the production
            const user = await this.findIncludedUnverifiedUser(email);
            if(user && user.verified){
                throw new ConflictException("This user already exist");
            }
            if(user && !user.verified){
                throw new ConflictException("You already have an account! Check your email to verify your account");
            }
            if(!validator.isLength(firstName,{ min: 3, max: undefined })) throw new BadRequestException("First Name Must Contain More Than 2 characters")
            if(!validator.isLength(lastName,{ min: 3, max: undefined })) throw new BadRequestException("Last Name Must Contain More Than 2 characters")
            if(!validator.isEmail(email)) throw new BadRequestException("You Have To Insert A Valid Email")
            if(!validator.isStrongPassword(password)) throw new BadRequestException("Password must contain at least 1 lowercase alphabetical character, 1 uppercase alphabetical character, 1 special character, 1 numerica character and at least 8 characters long")
           
            // Generate a token for the account email verification after
            const token = uuid();            
            // create user in db
            const newUser = new this.userModel({
                firstName,
                lastName,
                email,
                password, 
                token
            });
      
            // send confirmation mail
            await this.mailService.sendUserConfirmation(newUser, token, host);
            // Save the user to the database
            await newUser.save();

            return successResponse("User Added Successfully");
        } catch (error) {
            console.log(error)
            throw new InternalServerErrorException({
                message : error.message,
                error : error.error,
                status : "error"
            })
        }        
    }

    async updateUser(userId : string, data : any) : Promise<void>{
        try {
            // update the user informations
            await this.userModel.updateOne({_id : userId},{...data})
        } catch (error) {
            console.log(error)
        }
    }

    async updateManyUser(query : any, data : any) : Promise<void>{
        try {
            await this.userModel.updateMany({...query},{...data})
        } catch (error) {
            console.log(error)
        }
    }

    // Delete User Account
    async deleteUser ( user_id : string ) {
        try {
            await this.userModel.findByIdAndDelete(user_id);
            return successResponse("Account deleted successfully !")
        } catch (error) {
            throw new InternalServerErrorException({
                message : error.message,
                error : error.error,
                status : "error"
            })
        }
    }

    // Verify the user Email
    async verifyUserMail(token : string):Promise<ResponseTypes>{
        try {
            if(token){
                // Find a user with token
                const user = await this.userModel.findOne({token});
                if(user){
                    // Update User to Verified
                    await this.userModel.updateOne({token},{token : null, verified : true})
                    return successResponse("Your eamil is verified you can log into your account now!")
                }else{
                    throw new NotFoundException("User not found")
                }
            }else{
                throw new NotFoundException("User not found")
            }
            
        } catch (error) {
            throw new InternalServerErrorException({
                message : error.message,
                error : error.error,
                status : "error"
            })
        }
    }
}
