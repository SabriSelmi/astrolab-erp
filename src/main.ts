import { NestFactory, /* Reflector */} from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.enableCors();
  /*
  this is a way to inject the RoutesAuthGuard Globally

  const reflector = new Reflector()
  app.useGlobalGuards(new RoutesAuthGuard(reflector))


  the other way is in app.module.ts within provider
  
  providers: [AppService, {
    provide : APP_GUARD,
    useClass : RoutesAuthGuard
  }],

  */
  await app.listen(3001);
}
bootstrap();
