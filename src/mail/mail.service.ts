import { MailerService } from '@nestjs-modules/mailer';
import { Injectable } from '@nestjs/common';
import { User } from 'src/users/user.model';

@Injectable()
export class MailService {
    constructor(private mailerService: MailerService) {}

    async sendUserConfirmation(user: User, token: string, host : string) {
      const url = `${host}/users/verify?token=${token}`;
  
      await this.mailerService.sendMail({
        to: user.email,
        // from: '"Support Team" <support@example.com>', // override default from
        subject: 'Welcome to Money Wise! Please Confirm your Email :)',
        template: './confirmation', // `.hbs` extension is appended automatically
        context: { // ✏️ filling curly brackets with content
          name: user.firstName,
          url,
        },
      });
    }
}
