import { ResponseTypes } from "./types";

export function successResponse(message : string) : ResponseTypes {
    return {
        message,
        status : "success",
        error : null
    }
}

export const errorResponse = (message : string, error : string) : ResponseTypes => {
    return {
        message,
        status : "error",
        error 
    }
}


export function LowerCaseString(str : string) : string{
    return String(str).toLocaleLowerCase();
}

export function CapitalizeFirstLetter(str : string) : string {
    return String(str).charAt(0).toUpperCase() + String(str).slice(1).toLocaleLowerCase();
}

export const consolog = () => {
    return {
        info : (message : string, data? : any) => console.log('=============================\n','\x1b[34m','[INFO] : ', '\x1b[37m', message, "\n\n",JSON.stringify(data, null, 4),'\n============================='),
        error : (message : string, data? : any) => console.log('=============================\n','\x1b[31m','[ERROR] : ', '\x1b[37m', message, "\n\n",JSON.stringify(data, null, 4),'\n============================='),
        warning : (message : string, data? : any) => console.log('=============================\n','\x1b[33m','[WARNING] : ', '\x1b[37m', message, "\n\n",JSON.stringify(data, null, 4),'\n============================='),
        success : (message : string, data? : any) => console.log('=============================\n','\x1b[32m','[SUCCESS] : ', '\x1b[37m', message, "\n\n",JSON.stringify(data, null, 4),'\n============================='),
    }
}