import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { UsersModule } from 'src/users/users.module';
import { JwtModule } from '@nestjs/jwt/dist';
import { JwtStrategy } from './strategies/jwt.strategy';
import { LocalStrategy } from './strategies/local.strategy';
import { RtStrategy } from './strategies/rt.strategy';

@Module({
  imports : [UsersModule, 
  JwtModule.register({})],
  providers: [AuthService, JwtStrategy, LocalStrategy, RtStrategy],
  controllers: [AuthController]
})
export class AuthModule {}
