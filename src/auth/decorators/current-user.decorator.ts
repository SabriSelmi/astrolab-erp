import { createParamDecorator, ExecutionContext } from "@nestjs/common";

// Decorator to get the user from the request

export const CurrentUser = createParamDecorator(
    (_data : unknown, context : ExecutionContext)=>{
        if(context.getType() === "http"){
            return context.switchToHttp().getRequest().user;
        }
    }
)