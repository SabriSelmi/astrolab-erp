import { SetMetadata } from "@nestjs/common";

// Decorator to pass out the RoutesAuthGuard protected by jwt strategy
export const Public = () => SetMetadata("isPublic", true);