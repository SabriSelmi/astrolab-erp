import { createParamDecorator, ExecutionContext } from "@nestjs/common";

// Decorator to get the token from the request

export const CurrentToken = createParamDecorator(
    (_data : unknown, context : ExecutionContext)=>{
        if(context.getType() === "http"){
            const auth :string = context.switchToHttp().getRequest().headers.authorization;
            return auth.replace("Bearer ","")
        }
    }
)