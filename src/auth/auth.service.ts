import { ForbiddenException, Injectable, InternalServerErrorException, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config/dist/config.service';
import { JwtService } from '@nestjs/jwt';
import * as QRCode from "qrcode";
import * as speakeasy from "speakeasy";
import * as bcrypt from "bcrypt"
import { AtPayload, At_Rt, LoginType, RtPayload, SuccessErrorType } from 'src/types';
import { User } from 'src/users/user.model';
import { UsersService } from 'src/users/users.service';

@Injectable()
export class AuthService {
    constructor( 
        private readonly usersService : UsersService,
        private readonly jwtService : JwtService,
        private readonly configService : ConfigService
        ){}

    // Validate the user by email and password
    async validateUser(email : string, password : string) : Promise< User | null >{
        try {
            // Verify if the user exists verified or not verified
            const user = await this.usersService.findIncludedUnverifiedUser(email);
            if(!user) return null;
            if(user && !user.verified) throw new UnauthorizedException({
                message : "Check your email to verify your account first",
                error : "Unauthorized",
                status : "error"
            })
            // compare password
            const isValid = bcrypt.compareSync(password, user.password);
            return isValid? user : null
        } catch (error) {
            throw new InternalServerErrorException({
                message : error.message,
                error : error.error,
                status : "error"
            })
        }
    }

    async getTokens(userId : string, email : string) : Promise<At_Rt>{
        try {
            // Generate the access and refresh tokens
            const [access_token, refresh_token] = await Promise.all([
                this.jwtService.signAsync({
                    sub : userId,
                    email
                },
                {
                    secret : this.configService.get("JWT_SECRET"),
                    expiresIn : 60 * 15
                }
                ),
                this.jwtService.signAsync({
                    sub : userId,
                    email
                },
                {
                    secret : this.configService.get("JWT_REFRESH_SECRET"),
                    expiresIn : 60 * 60 * 24 * 7
                }
                )
            ])
            return {
                access_token, refresh_token
            }
        } catch (error) {
            throw new InternalServerErrorException({
                message : error.message,
                error : error.error,
                status : "error"
            })
        }
        
    }

    // Function to execute before the user login
    async preLogin(user : User) : Promise<LoginType | {
        status: string;
        redirectTwoFactors: boolean;
    }>{
        // Verify if the user uses the two factors auth or no
        if(user.twoFactors){
            return {
                status : "success",
                redirectTwoFactors : true
            }
        }else{
            // Login the user if he don't use the two factors auth
            return this.login(user)
        }
    }

    // Verify two factors auth
    async verifyTwoFactorAndLogin(token : string, user : User){
        try {
            const email = user.email;
            // find user with email
            const userData = await this.usersService.findUser(email);
            // get the secret from the user data
            const { base32: secret} = userData.temp_secret;

            // verifying the token
            const verified = speakeasy.totp.verify({
                secret,
                encoding: 'base32',
                token
            });
            if(verified){
                return this.login(user)
            }else{
                return {verified, status : "error"}
            }
            
        } catch (error) {
            throw new InternalServerErrorException({
                message : error.message,
                error : error.error,
                status : "error"
            })
        }
        
    }

    // Login function
    async login(user : User) : Promise<LoginType>{
        try {
            // generate tokens
            const tokens = await this.getTokens(user._id, user.email);
            // update the refresh token hash in the user document database
            await this.updateRtHash(user._id, tokens.refresh_token);
            
            return {...tokens, 
                email : user.email, 
                firstName : user.firstName, 
                lastName : user.lastName,
                twoFactors : user.twoFactors,
                status : "success"}
        } catch (error) {
            throw new InternalServerErrorException({
                message : error.message,
                error : error.error,
                status : "error"
            })
        }
        
    }

    // Logout function
    async logout ( userId : string) : Promise<{status:SuccessErrorType}>{
        try {
            // delete the refresh token from the user database document
            await this.usersService.updateManyUser(
                {
                    $and : 
                    [
                        {_id : userId},
                        {refresh_token : {$ne : null}}
                    ]
                },
                {
                    refresh_token : null
                }
                );
                return {status : "success"}
        } catch (error) {
            throw new InternalServerErrorException({
                message : error.message,
                error : error.error,
                status : "error"
            })
        }
    }

    // Refresh the access token
    async refreshToken(userId : string, rt : string) : Promise<LoginType>{
        try {
            // Find user with id
            const user = await this.usersService.findUserById(userId);
            if(!user) throw new ForbiddenException();
            // Verify if the refresh token from request match the user refresh token on the database
            const rtMatch = bcrypt.compareSync(rt,user.refresh_token);
            if(!rtMatch) throw new ForbiddenException();
            // recreate the access and refresh token and store them to the user database
            const tokens = await this.getTokens(user._id, user.email);
            await this.updateRtHash(user._id, tokens.refresh_token);
            return {...tokens, 
                    email : user.email, 
                    firstName : user.firstName, 
                    lastName : user.lastName,
                    twoFactors : user.twoFactors,
                    status : "success"
                }
        } catch (error) {
            throw new InternalServerErrorException({
                message : error.message,
                error : error.error,
                status : "error"
            })
        }
    }

    // Store the refresh token to the user document database
    async updateRtHash(userId : string, rt : string) : Promise<void>{
        try {
            // generate salt
            const salt = bcrypt.genSaltSync(10);
            // generate hash
            const hash = bcrypt.hashSync(rt, salt);
            await this.usersService.updateUser(userId, {refresh_token : hash});
        } catch (error) {
            console.log(error)
        }
       
    }

    // Verify the token
    async verifyToken(token : string) : Promise<Partial<User> & {status : "success" | "error"}>{
        try {
            // Verify the token with jwtService
            const decoded = this.jwtService.verify(token,{
            secret : this.configService.get("JWT_SECRET")
            })
            // Find verified user with email
            const user = await this.usersService.findUser(decoded.email);
            if(!user) throw new NotFoundException("User Not Found");
            return {
                email : user.email, 
                firstName : user.firstName, 
                lastName : user.lastName,
                twoFactors : user.twoFactors,
                status : "success"
            }
        } catch (error) {
            throw new InternalServerErrorException({
                message : error.message,
                error : error.error,
                status : "error"
            })
        }
        
    }
    async setTwoFactors (user : AtPayload) : Promise<{qrcode : string}> {
        try {
            const userId = user.sub;
            // generate secret key 
            const temp_secret = speakeasy.generateSecret({name : "Money-Wise"});
            // transform the secret key into a qrcode
            const qrcode = await QRCode.toDataURL(temp_secret.otpauth_url);
            // Save the secret key to the user document
            await this.usersService.updateUser(userId , {temp_secret, data_url : qrcode});
            return {qrcode}
        } catch (error) {
            throw new InternalServerErrorException({
                message : error.message,
                error : error.error,
                status : "error"
            })
        }
    }

    async verifyTwoFactor(token : string, user : RtPayload){
        try {
            const email = user.email;
            const userData = await this.usersService.findUser(email);
            const { base32: secret} = userData.temp_secret;

            // verifying the token
            const verified = speakeasy.totp.verify({
                secret,
                encoding: 'base32',
                token
            });
            if(verified){
                await this.usersService.updateUser(user.sub,{twoFactors : true})
            }
            return {verified, status : "success"}
        } catch (error) {
            throw new InternalServerErrorException({
                message : error.message,
                error : error.error,
                status : "error"
            })
        }
        
    }

    async deleteTwoFactor(user : RtPayload){
        try {
            const _id = user.sub;
            // update user collection
            await this.usersService.updateUser(_id,{twoFactors : false})
            return {status : "success", message : "Two Factors Authentication is removed"}
        } catch (error) {
            throw new InternalServerErrorException({
                message : error.message,
                error : error.error,
                status : "error"
            })
        }
        
    }
}
