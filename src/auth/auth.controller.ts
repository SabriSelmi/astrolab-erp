import { Controller, Post, Body, UseGuards } from '@nestjs/common';
import { User } from 'src/users/user.model';
import { AuthService } from './auth.service';
import { LocalAuthGuard } from './guards/local.authguard';
import { RoutesAuthGuard } from './guards/jwt.authguard';
import { CurrentUser, Public } from './decorators';
import { RefreshAuthGuard } from './guards/refresh_jwt.authguard';
import { AtPayload, LoginType, RtPayload } from 'src/types';
import { CurrentToken } from './decorators/access-token-decorator';

@Controller('auth')
export class AuthController {
    constructor(private readonly authService : AuthService){}

    @Public() // bypass the RoutesAuthGuard
    @UseGuards(LocalAuthGuard) // Local Auth Guard Decorator
    @Post("pre-login")
    preLogin(@CurrentUser() user : User) : Promise<LoginType | {
        status: string;
        redirectTwoFactors: boolean;
    }>{
        return this.authService.preLogin(user)
    }

    @Public() // bypass the RoutesAuthGuard
    @UseGuards(LocalAuthGuard) // Local Auth Guard Decorator
    @Post("verify-2fa-login")
    verifyTwoFactorLogin(@CurrentUser() user : User, @Body("token") token : string){
        return this.authService.verifyTwoFactorAndLogin(token,user);
    }

    @Post("logout")
    logout(@CurrentUser() user : AtPayload){
        return this.authService.logout(user.sub);
    }

    @Public()
    @UseGuards(RefreshAuthGuard) // Refresh Auth Guard Decorator to refresh the access token
    @Post("refresh")
    refresh(@CurrentUser() user : RtPayload){
        return this.authService.refreshToken(user.sub,user.refresh_token);
    }

    @Public()
    @Post("verify") // Verify the access token
    verifyToken(@CurrentToken() token : string){
        return this.authService.verifyToken(token)
    }

    @UseGuards(RoutesAuthGuard)
    @Post("2fa") // Set Two Factors Authentication
    twoFactorAuth(@CurrentUser() user : RtPayload){
        return this.authService.setTwoFactors(user)
    }

    @UseGuards(RoutesAuthGuard)
    @Post("verify-2fa") // Verify the 6 digits code
    verifyTwoFactor(@CurrentUser() user : RtPayload, @Body("token") token : string){
        return this.authService.verifyTwoFactor(token,user);
    }

    @UseGuards(RoutesAuthGuard)
    @Post("delete-2fa") // Delete the two factors authentication
    deleteTwoFactor(@CurrentUser() user : RtPayload){
        return this.authService.deleteTwoFactor(user);
    }

}
