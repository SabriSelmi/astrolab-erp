import {AuthGuard} from "@nestjs/passport";

// Refresh Token AuthGuard
export class RefreshAuthGuard extends AuthGuard("jwt-refresh"){}