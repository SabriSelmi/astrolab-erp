import {AuthGuard} from "@nestjs/passport";

// Local AuthGuard
export class LocalAuthGuard extends AuthGuard("local"){}