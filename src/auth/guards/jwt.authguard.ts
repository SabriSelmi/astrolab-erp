import { ExecutionContext, Injectable } from "@nestjs/common";
import { Reflector } from "@nestjs/core";
import {AuthGuard} from "@nestjs/passport";

// Handle the Public decorator to bypass the guard
@Injectable()
export class RoutesAuthGuard extends AuthGuard("jwt"){
    constructor(private reflector : Reflector){
        super();
    }

    canActivate(context : ExecutionContext){
        const isPublic = this.reflector.getAllAndOverride("isPublic",[
            context.getHandler(),
            context.getClass()
        ])
        if(isPublic) return true;
        return super.canActivate(context); 
    }
}