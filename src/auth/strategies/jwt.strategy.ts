import { ForbiddenException, Injectable, InternalServerErrorException } from "@nestjs/common";
import {PassportStrategy} from "@nestjs/passport";
import { ExtractJwt, Strategy } from "passport-jwt";
import { UsersService } from "src/users/users.service";
import { ConfigService } from '@nestjs/config/dist/config.service';
import { AtPayload } from "src/types";


@Injectable()

// Implementing Jwt Strategy
export class JwtStrategy extends PassportStrategy(Strategy, "jwt"){
    constructor(
        private readonly usersService : UsersService,
        private readonly configService : ConfigService
        ){
        super({
            jwtFromRequest : ExtractJwt.fromAuthHeaderAsBearerToken(),
            ignoreExpiration : false,
            secretOrKey : configService.get("JWT_SECRET")
        })
    }

    // Validate user by email
    async validate(validationPayload : AtPayload) : Promise<AtPayload>{
        try {
            const user = await this.usersService.findUser(validationPayload.email);
            if(!user) throw new ForbiddenException();
            return validationPayload
        } catch (error) {
            throw new InternalServerErrorException({
                message : error.message,
                error : error.error,
                status : "error"
            })
        }
    }
}