import { Injectable, InternalServerErrorException, UnauthorizedException } from "@nestjs/common";
import {PassportStrategy} from "@nestjs/passport";
import { Strategy } from "passport-local";
import { User } from "src/users/user.model";
import { AuthService } from "../auth.service";

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy){
    constructor(private readonly authService : AuthService){
        super({usernameField : "email"}) // If you are using email for validation you must specify the usernameField
    }

    // Validate Use with E-mail and password
    async validate(email : string, password : string) : Promise<User>{
        try {
            const user = await this.authService.validateUser(email, password);
            if(!user) throw new UnauthorizedException();
            return user
        } catch (error) {
            throw new InternalServerErrorException({
                message : error.message,
                error : error.error,
                status : "error"
            })
        }
        
    }
}