export enum TypesType {
    fix,
    variable
}
export enum FrequenceType{
    daily,
    weekly,
    monthly,
    yearly
}

export type ResponseTypes = {
    message : string;
    status : string;
    error : string | null;
}