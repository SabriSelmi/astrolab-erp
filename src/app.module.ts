import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { MailModule } from './mail/mail.module';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { APP_GUARD } from '@nestjs/core';
import { RoutesAuthGuard } from './auth/guards/jwt.authguard';

@Module({
  imports: [ConfigModule.forRoot({
    isGlobal : true
  }),
  MongooseModule.forRootAsync({
    useFactory: async (config: ConfigService) => ({
    uri : `mongodb+srv://${config.get("MONGO_USER")}:${config.get("MONGO_PASS")}@${config.get("DATABASE_LINK")}`,

    }),
      inject: [ConfigService],
  }), 
  AuthModule, 
  UsersModule, 
  MailModule],
  providers: [{
    provide : APP_GUARD,
    useClass : RoutesAuthGuard
  }],
})
export class AppModule {}
